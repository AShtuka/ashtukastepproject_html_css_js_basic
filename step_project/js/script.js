/*___________________GENERAL_____________________*/

const changeTab = (tabName, tabContainer, contentContainer) => {
    if (!tabName) return;
    let tab = document.querySelector(`.${tabContainer} > .active`);
    tab.classList.remove("active");
    document.querySelector(`.${tabContainer} > [data-name=${tabName}]`).classList.add("active");
    if (tabName === "all") {
        document.querySelectorAll(`.${contentContainer}`).forEach((item) => item.dataset.display = "true");
    } else {
        let showedItems = document.querySelectorAll(`.${contentContainer}[data-display='true']`);
        showedItems.forEach((item) => item.dataset.display = "false");
        let itemsForShow = document.querySelectorAll(`.${contentContainer}[data-name=${tabName}]`);
        itemsForShow.forEach((item) => item.dataset.display = "true");
    }
}

function makeCounter(lastIndex) {
    let currentCount = 0;
    return function() {
        if (currentCount === lastIndex) currentCount = 0;
        return currentCount++;
    };
}

const preLoaderFunc = (event, loadFunc, preLoaderContainer, counter) => {
    event.preventDefault();
    preLoaderContainer.style.display = "flex";
    setTimeout(() => {
        preLoaderContainer.style.display = "";
        loadFunc();
        if (counter() === 1) {
            preLoaderContainer.nextElementSibling.style.display = "none";
        }
    }, 3000);
}

/*______________OUR_SERVICES_BLOCK___________ */

const tabs = document.querySelector(".our-services-menu");
const contentContainerServiceBlock = "display";
const tabContainerServiceBlock = "our-services-menu";
tabs.addEventListener("click", (event) => {
    const tabName = event.target.dataset.name;
    changeTab(tabName, tabContainerServiceBlock, contentContainerServiceBlock);
})

/* _________________AMAZING_WORK_BLOCK________________*/

const amazingWorkTabs = document.querySelector(".amazing-work-menu");
const gallery = document.querySelectorAll(".amazing-work-img-wr .amazing-work-img");
const amazingWorkBtn = document.querySelector(".load-btn").children[1];
const categoryNameArr = ["graphic-design", "web-design", "landing-page", "wordpress"];
const tabContainerAmazingWork = "amazing-work-menu";
const contentContainerAmazingWork = "amazing-work-hover";

const createAmazingWorkHover = () => {
    const faShare = document.createElement("i");
    faShare.classList.add("fa");
    faShare.classList.add("fa-share-alt");
    faShare.classList.add("fa-lg");
    faShare.classList.add("share-pos");
    const faSearch = document.createElement("i");
    faSearch.classList.add("fa");
    faSearch.classList.add("fa-search");
    faSearch.classList.add("fa-lg");
    faSearch.classList.add("search-pos");
    const shareIc = document.createElement("div");
    shareIc.classList.add("share-ic");
    shareIc.appendChild(faShare);
    const searchIc = document.createElement("div");
    searchIc.classList.add("search-ic");
    searchIc.appendChild(faSearch);
    const icon = document.createElement("div");
    icon.classList.add("icon");
    icon.appendChild(shareIc);
    icon.appendChild(searchIc);
    const title = document.createElement("p");
    title.classList.add("amazing-work-img-cover-title");
    title.innerText = "creative design";
    const subtitle = document.createElement("p");
    subtitle.classList.add("amazing-work-img-cover-subtitle");
    subtitle.innerText = "web design";
    const cover = document.createElement("div");
    cover.classList.add("cover-item-amazing-work-img");
    cover.appendChild(icon);
    cover.appendChild(title);
    cover.appendChild(subtitle);
    const hover = document.createElement("div");
    hover.classList.add("amazing-work-hover");
    hover.setAttribute("data-display", "true");
    hover.appendChild(cover);
    return hover;
}

const counterForCategoryName = makeCounter(4);

const createCategoryName = (categoryArr) => {
    return categoryArr[counterForCategoryName()];
}

const wrapImgForHover = (imgArr, categoryName) => {
    const fragment = document.createDocumentFragment();
    imgArr.forEach((item) => {
        const hover = createAmazingWorkHover();
        hover.appendChild(item);
        let name;
        if (categoryName) {
            name = categoryName;
        } else {
            name = createCategoryName(categoryNameArr);
        }
        hover.setAttribute("data-name", name);
        hover.querySelector(".amazing-work-img-cover-subtitle").innerHTML = name;
        fragment.appendChild(hover);
    })
    document.querySelector(".amazing-work-img-wr").appendChild(fragment);
}

wrapImgForHover(gallery);

amazingWorkTabs.addEventListener("click", (event) => {
    const tabName = event.target.dataset.name;
    changeTab(tabName, tabContainerAmazingWork, contentContainerAmazingWork);
})

const counterForImg = makeCounter(23);

const createImg = () => {
    const imgArr =[];
    for (let i =0; i < 12; i++) {
        const img = document.createElement("img");
        img.classList.add("amazing-work-img");
        img.src = `img/amazingWorkImg/${counterForImg() + 1}.jpg`;
        img.alt = "IMG NOT FOUND";
        imgArr.push(img);
    }
    return imgArr;
}

const loadImg = () => {
    const imgArr = createImg();
    let categoryName = document.querySelector(".amazing-work-menu > .active").dataset.name;
    if (categoryName === "all") {
        wrapImgForHover(imgArr)
    } else {
        wrapImgForHover(imgArr, categoryName)
    }
    changeTab(categoryName, tabContainerAmazingWork, contentContainerAmazingWork);
}

const preLoader = document.querySelector(".preloader-container");
const counterForPreLoader = makeCounter();

amazingWorkBtn.addEventListener("click", (event) => {
    preLoaderFunc(event, loadImg, preLoader, counterForPreLoader);
}, loadImg);

/*_____________________SAY_ABOUT_HAM_BLOCK______________________*/

const userImgWr  = document.querySelector(".user-img-wr");
const arrowClick  = document.querySelector(".user-photo-container");
const contentContainerHam = "block-for-change";
const tabContainerHam = "user-img-wr";

userImgWr.addEventListener("mouseover", (event) => {
    const tabName = event.target.dataset.name;
    changeTab(tabName, tabContainerHam, contentContainerHam);
})

const findActiveTabs = (tabsList) => {
    let index = 0;
    for (index; index < tabsList.length; index++) {
        if(tabsList[index].className.includes("active")) {
            return index;
        }
    }
}

const findIndex = (moveDirection, index, arrLength) => {
    let result = index;
    if(moveDirection === "arrow-right") {
        if (index === arrLength -1) {
            result = 0;
        } else {
            result++;
        }
    } else {
        if (index === 0) {
            result = arrLength - 1;
        } else {
            result--;
        }
    }
    return result;
}

arrowClick.addEventListener("click", (event) => {
    let arrowDirection = event.target.classList[0];
    if (arrowDirection !== "arrow" && arrowDirection !== "arrow-right" && arrowDirection !== "arrow-left") return;
    if (arrowDirection === "arrow") arrowDirection = event.target.children[0].className;
    const indexOfItem = findActiveTabs(userImgWr.children);
    const index = findIndex(arrowDirection, indexOfItem, userImgWr.children.length);
    const tabName = userImgWr.children[index].dataset.name;
    changeTab(tabName, tabContainerHam, contentContainerHam);
})


/*__________________MASONRY_GALLERY_BLOCK______________________*/

const size = ".size";
const elem = document.getElementById("masonry-gallery");

const masonryItemHover = () => {
    const arrowAlt = document.createElement("i");
    arrowAlt.classList.add("fa");
    arrowAlt.classList.add("fa-arrows-alt");
    arrowAlt.classList.add("fa-2x");
    const arrowAltCont = document.createElement("p");
    arrowAltCont.classList.add("hover-icon");
    arrowAltCont.appendChild(arrowAlt);
    const search = document.createElement("i");
    search.classList.add("fa");
    search.classList.add("fa-search");
    search.classList.add("fa-2x");
    const searchCont = document.createElement("p");
    searchCont.classList.add("hover-icon");
    searchCont.appendChild(search);
    const faIconWr = document.createElement("div");
    faIconWr.classList.add("fa-icon-wr");
    faIconWr.appendChild(arrowAltCont);
    faIconWr.appendChild(searchCont);
    const coverItem = document.createElement("div");
    coverItem.classList.add("cover-item");
    coverItem.appendChild(faIconWr);
    const masonryItem = document.createElement("div");
    masonryItem.classList.add("masonry-item");
    masonryItem.classList.add("size");
    masonryItem.appendChild(coverItem);
    return masonryItem;
}

const  coverMasonryItem = (gallery) => {
    const masonryItems = []; // need for masonry plugin
    const fragment = document.createDocumentFragment();
    gallery.forEach((item) => {
        const hover = masonryItemHover();
        hover.appendChild(item);
        masonryItems.push(hover);
        fragment.appendChild(hover);
    })
    elem.appendChild(fragment);
    return masonryItems;
}

const masonryGallery = document.querySelectorAll("#masonry-gallery img");

coverMasonryItem(masonryGallery);

const masonry = new Masonry( elem, {
    itemSelector: ".masonry-item",
    columnWidth: size,
    percentPosition: true
});

const galleryBtn = document.getElementById("galleryBtn");
const galleryPreLoader = document.querySelector(".gallery-preloader-container");
const galleryMain = ["img/masonryGallery/10.png", "img/masonryGallery/21.png",
    "img/masonryGallery/22.png", "img/masonryGallery/23.png",
    "img/masonryGallery/24.png", "img/masonryGallery/25.png",
    "img/masonryGallery/26.png", "img/masonryGallery/27.png",
    "img/masonryGallery/31.png"];

const masonryLoadFunc = () => {
    const gallery = [];
    galleryMain.forEach((item) => {
        const img = document.createElement("img");
        img.src = item;
        gallery.push(img);
    })
    const addedItems = coverMasonryItem(gallery);
    masonry.appended(addedItems);
}

const masonryCounterForPreLoader = makeCounter();

galleryBtn.addEventListener("click", (event) => {
   preLoaderFunc(event, masonryLoadFunc, galleryPreLoader, masonryCounterForPreLoader);
}, masonryLoadFunc);

